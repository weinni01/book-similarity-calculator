Book Similarity Calculator

1). 
		The problem presented was to implement a book similarity calculator. This program would request access to a book api, such as Google Books, and compare the books using a certain criteria specified by the programmer.  
	For my solution I decided to use Google Books API. For the criteria I used to compute the score, I decided to compare the book’s country, page count, language, publisher, print type, and author. I chose these properties 
	because I felt that these were the attributes that a reader would be most interested in when comparing two books. Furthermore, these attributes directly impact a reader’s experience. For example, a reader may be looking 
	for books that have the same author and language. All they have to do is input the ISBNs of both books to be able to see if a book they are looking at is similar to the one they have already read. Additionally, I decided 
	to make the input parameters for the calculator the ISBNs of the books. I chose the ISBNs since these are the unique identifiers for books and it eliminates the possibility of the API returning more than one book. 			Validity of the program is tested in the main method. I would chose books that had certain criteria when compared to each other. For example, I have a test case where both books have the same author and another one tests 	 when the books have the same publishers, etc.

2). 
		Since a front end was not necessary, I decided to make a java application. While this required the program to become more expensive with string builders and input streams, I believe that this made the code simpler, 
	easier to follow, and easier to scale. Angular CLI is great for build front end heavy applications, but once this is not required, it over complicates code. Making the application a java program allows for simple code, 
	but connecting it to a front end in the future is as simple as making rest endpoints. Since the code is relatively simple, it allows for additional similarity criteria to be added with ease. This essentially gave me 		the best of both worlds with simple code and scalability. 

3). 
	Future improvements would include connecting the application to a simple front end. Connecting a front end could be accomplished by exposing the functions via a rest endpoints. 
	This would allow, for example, an Angular program to access the service using JavaScript calls. A user would be able to input the ISBNs of both books on a webpage and the calculator would return the similarity score. 
	Additionally, I would like to implement a system that not only returns a similarity score, but a less primitive description of why the books received such a score. The program currently prints a string of the attributes 	matched, but I would like to implement a matrix that returns which values matched and which values were different. This would help the user easily see what attributes match and allow them to make a more informed decision 	 when picking a book.
4). 
	•	Import the code to an IDE as a Maven project
	•	Import the org.json package from maven (should not be unnecessary if POM is downloaded, but sometimes it can get messed up)
	•	Run code using IDE “run” button
	•	Comments for test cases can be found in the main method 


