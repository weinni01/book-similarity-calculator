package book.similarity;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BookSimCalc {

	/**
	 * Used to read through and parse a JSON object
	 * @param rd     Reader to read through JSON object
	 * @return sb    String builder that contains all info from JSON object
	 * @throws IOException
	 */
	private static String readJsonObject(Reader read) throws IOException {
		StringBuilder string = new StringBuilder();
		int counter = 0;
		while (counter != -1) {
			counter = read.read();
			string.append((char) counter);
		}
		return string.toString();
	}

	/**
	 * Retrieves the data from the given url endpoint
	 * @param url     URL to retrieve data from
	 * @return json   json object contain all info from endpoint
	 * @throws IOException
	 * @throws JSONException
	 */
	public static JSONObject readURLInfo(String url) throws IOException, JSONException {
		System.setProperty("http.agent", "Chrome");
		InputStream is = new URL(url).openStream();

		try {
			BufferedReader read = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
			String jsonText = readJsonObject(read);
			JSONObject json = new JSONObject(jsonText);
			return json;
		} 
		finally {
			is.close();
		}
	}

	/**
	 * Compares two books based on the isbns that the user inputs
	 * @param  isbn1    First ISBN to be compared 
	 * @param  isbn2    Second ISBN to be compared
	 * @return grade    Similarity score 
	 * @throws IOException
	 * @throws JSONException
	 */
	public static double compareBooks(String isbn1, String isbn2) throws IOException, JSONException{
		double grade = 0; 

		JSONObject json = readURLInfo("https://www.googleapis.com/books/v1/volumes?q=isbn:"+isbn1); 
		JSONObject json2 = readURLInfo("https://www.googleapis.com/books/v1/volumes?q=isbn:"+isbn2); 

		JSONArray items = (JSONArray) json.get("items"); 
		JSONArray items2 = (JSONArray) json2.get("items");

		//returns country of both books
		String country1 = items.getJSONObject(0).getJSONObject("saleInfo").getString("country"); 
		String country2 = items2.getJSONObject(0).getJSONObject("saleInfo").getString("country"); 

		//returns page count of each book
		Object pageCount1 = items.getJSONObject(0).getJSONObject("volumeInfo").get("pageCount");
		Object pageCount2 = items2.getJSONObject(0).getJSONObject("volumeInfo").get("pageCount");

		//returns language books were printed in
		Object lang1 = items.getJSONObject(0).getJSONObject("volumeInfo").get("language"); 
		Object lang2 = items2.getJSONObject(0).getJSONObject("volumeInfo").get("language");

		//returns publisher of books
		Object publisher1 = items.getJSONObject(0).getJSONObject("volumeInfo").get("publisher");
		Object publisher2 = items2.getJSONObject(0).getJSONObject("volumeInfo").get("publisher");

		//returns print type of books
		Object printType1 = items.getJSONObject(0).getJSONObject("volumeInfo").get("printType"); 
		Object printType2 = items2.getJSONObject(0).getJSONObject("volumeInfo").get("printType");

		Object authors1 = items.getJSONObject(0).getJSONObject("volumeInfo").getJSONArray("authors").getString(0);
		Object authors2 = items2.getJSONObject(0).getJSONObject("volumeInfo").getJSONArray("authors").getString(0);

		//String that will keep track of attributes matched
		String str = new String(); 

		if(isbn1.equals(isbn2)){
			grade = 100; 
			str = "ISBN Matched: Books are the same";
			System.out.println(str); 
			return grade; 
		}

		if(country1.equals(country2)){
			grade++; 
			str += "country, "; 
		}
		if(pageCount1.equals(pageCount2)){
			grade++; 
			str += "page count, "; 
		}
		if(lang1.equals(lang2)){
			grade++; 
			str += "language, ";
		}
		if(publisher1.equals(publisher2)){
			grade++; 
			str += "publisher, ";
		}
		if(printType1.equals(printType2)){
			grade++; 
			str += "print type, ";
		}

		if(authors1.equals(authors2)){
			grade++; 
			str += "authors, ";
		}
		
		System.out.println("Properties Matched: " + str);
		return grade/6 * 100; 
	}

	public static void main(String[] args) throws IOException, JSONException {

		//General compare
		double score = compareBooks("1118063333", "0439708184"); 
		System.out.println(score);
		System.out.println("-------------------------------------------------------");

		//Comparing the same book
		double isbnMatch = compareBooks("1118063333", "1118063333"); 
		System.out.println(isbnMatch);
		System.out.println("-------------------------------------------------------");

		//books with same languages 
		double diffLang = compareBooks("0525552812", "0151010269"); 
		System.out.println(diffLang);
		System.out.println("-------------------------------------------------------");

		//books with same author
		double sameAuths = compareBooks("0151010269", "0156767503");
		System.out.println(sameAuths);
		System.out.println("-------------------------------------------------------");

		//publishers match, but same book
		double samePubs = compareBooks("0451527313", "0451527291");
		System.out.println(samePubs);
		System.out.println("-------------------------------------------------------");
	}
}
